// Requires
const app = require("./backend/app");
const debug = require("debug")("node-angular");
const http = require("http")

// Port normalization
const normalizePort = val => {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
};

// onError definition
const onError = error => {
    if (error.syscall !== "listen") {
        throw error;
    }
    const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
    switch (error.code) {
        // Try sudo or equivalente if EACCES
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        // The server is already running?
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        // Any other case
        default:
            throw error;
    }
};

// onLIstening definition
const onListening = () => {
    const addr = server.address();
    const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
    debug("Listening on " + bind);
};

// normalize port
const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

const server = http.createServer(app);
server.on("error", onError);
server.on("listening", onListening);
server.listen(port, () => {
  console.log("Listening...");
});
