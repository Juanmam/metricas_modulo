// Requires
const express = require('express');
const app = express();

// Routes
const metricRoutes = require('./api/routes/metric-routes');

// Base URL routes
const metricBaseRoute = '/tugocercadeti/api/metrics';

// Metric Routes
app.use(`${metricBaseRoute}`, metricRoutes);

module.exports = app;
