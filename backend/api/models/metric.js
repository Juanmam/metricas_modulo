const mongoose = require('mongoose');

const metricSchema = mongoose.Schema({
    key: { type: String, require: true },
    value: { type: String, require: false }
});

module.exports = mongoose.model('Metric', metricSchema, 'metric')
