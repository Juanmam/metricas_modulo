const mongoose = require('mongoose');

const metricsSchema = mongoose.Schema({
    metric: { type: String, require: true },
    username: { type: String, require: false },
    values: [{
      type: mongoose.Schema.Types.ObjectId, ref: 'Metric'
    }]
});

module.exports = mongoose.model('Metrics', metricsSchema, 'metrics')
