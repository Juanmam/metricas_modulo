//Requires
const express = require('express');
const router = express.Router();

//Models
const modelController = require('../controllers/metric-controller');

////////////////////API////////////////////

//CaptureMetric
router.post('/', modelController.capture);

//AppOpened
router.get('/getMetric', modelController.getMetric);

router.delete('/cleanAll', modelController.cleanAll);

module.exports = router;
