///REQUIRES
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

///MODELS
const Metric = require('../models/metric');
const Metrics = require('../models/metrics');



//// API

/// Capture: Takes the request, mainly a metric name and a set of pair (Key, value).
exports.capture = (req, res, next) => {
    // Get request and parse it into something that we can use.
    let request_object = JSON.parse(req.body.data);

    // We will use this to store the pairs (Key, Value)
    let metric_array = [];

    // We loop around the request object and get the metrics
request_object.values.forEach(function(metric){
  // Create a new metric
  let newMetric = new Metric({
     key: metric.key,
     value: metric.value
   });

  // Lets save the metric
  newMetric.save()

  // Add metric to array.
  metric_array.push(newMetric);
});


    let newMetrics = new Metrics({
	     metric: request_object.metric,
       username: request_object.username,
	     values: metric_array
    });

    newMetrics.save().then(result => {
	res.status(200).json({
	    message: 'Metric Created',
	    result
	});
    }).catch(err => {
	res.status(500).json({
	    error: err,
	    message: "Error Saved"
	});
    });

}

///

exports.getMetric = (req, res, next) => {
    var request_object = JSON.parse(req.body.data);

    Metrics.find({'name' : request_object.name})
    .populate('values')
    .exec( function(err, values){
      if(err) console.log(err);
      else { console.log(values);
        res.status(200).json({values})
      }
    });
}

///
exports.cleanAll = (req, res, next) => {
    Metrics.deleteMany({}, (err, data) => {
	if (err){
	    res.send(err)
	} else {
	    res.send(data)
	}
    });
}
